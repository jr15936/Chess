#include "testMove.h"
#include "piece.h"

void castleKingside(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i)
{
    board[cur_pos_i][6] = board[cur_pos_i][4];

    board[cur_pos_i][5] = board[cur_pos_i][7];
    board[cur_pos_i][4].setEmpty();
    board[cur_pos_i][7].setEmpty();
}

void castleQueenside(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i)
{
    board[cur_pos_i][2] = board[cur_pos_i][4];
    board[cur_pos_i][3] = board[cur_pos_i][0];
    board[cur_pos_i][4].setEmpty();
    board[cur_pos_i][0].setEmpty();
}

void moveKing(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (cur_pos_j == 4 && new_pos_j == 6) {
        castleKingside(board, cur_pos_i);
    }
    else if (cur_pos_j == 4 && new_pos_j == 2) {
        castleQueenside(board, cur_pos_i);
    }
    //Preforms regular king move
    else {
        board[new_pos_i][new_pos_j] = board[cur_pos_i][cur_pos_j];
        board[cur_pos_i][cur_pos_j].setEmpty();
    }
}

void promotePawn(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    //TODO: implement this function properly
    char c;
    if (cur_pos_i == 6) {
        c = 'b';
    }
    else {
        c = 'w';
    }
    board[new_pos_i][new_pos_j] = TestPiece(false,c,'q');
    board[cur_pos_i][cur_pos_j].setEmpty();
}

void enPassant(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    board[new_pos_i][new_pos_j] = board[cur_pos_i][cur_pos_j];
    board[cur_pos_i][cur_pos_j].setEmpty();
    board[cur_pos_i][new_pos_j].setEmpty();
}

void movePawn(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (cur_pos_j != new_pos_j && board[new_pos_i][new_pos_j].empty) {
        enPassant(board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
    }
    else if (((cur_pos_i == 1 && new_pos_i == 0) && board[cur_pos_i][cur_pos_j].colour == 'w') ||
              (cur_pos_i == 6 && new_pos_i == 7 && board[cur_pos_i][cur_pos_j].colour == 'b')) {
        promotePawn(board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
    }
    else {
        board[new_pos_i][new_pos_j] = board[cur_pos_i][cur_pos_j];
        board[cur_pos_i][cur_pos_j].setEmpty();
    }
}

void testMove(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (board[cur_pos_i][cur_pos_j].type == 'k') {
        moveKing(board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
    }
    //deals with special pawn moves: en passant and promotion
    else if (board[cur_pos_i][cur_pos_j].type == 'p') {
        movePawn(board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
    }
    else {
        board[new_pos_i][new_pos_j] = board[cur_pos_i][cur_pos_j];
        board[cur_pos_i][cur_pos_j].setEmpty();
    }
}
