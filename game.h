#ifndef GAME_H
#define GAME_H

#include "piece.h"
#include "turnInfo.h"

class Game : public QFrame
{
    Q_OBJECT

public:

    std::vector<std::vector<Piece *>> board_pieces;
    int turn_no;
    int fifty_move;
    char current_player;
    bool black_kingside, black_queenside, white_kingside, white_queenside;
    bool normal;
    QPoint *cur_pos;
    QPoint *new_pos;
    QPoint *white_king_pos;
    QPoint *black_king_pos;
    std::vector<std::vector<TestPiece>> test_board;
    std::vector<TurnInfo> turn_list;
    QLabel *cur_h, *new_h, *last_h;
    QLabel *end_msg;
    QLabel *hint_cur, *hint_new;


    void nextPlayer();
    void printBoard();
    void turn();

    explicit Game(QWidget *parent = nullptr);

protected:
    void mousePressEvent(QMouseEvent *event) override;

signals:
    void prevSidebarSignal(char colour);
    void nextSidebarSignal(char colour);
    void resetSidebarSignal();
    void updateTakenPieces(TestPiece piece);
    void moveListSignal(QString moveString, int turn_no);

private slots:
    void resetGame();
    void showHint();
    void prevTurn();
    void nextTurn();


};

#endif // GAME_H
