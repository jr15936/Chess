#include "pawnpromotion.h"
#include "game.h"
#include "piece.h"
#include "isGameOver.h"
#include "algebraicnotation.h"
#include "validMove.h"
#include <QLabel>
#include <QMouseEvent>
#include <QGraphicsDropShadowEffect>

PawnPromotion::PawnPromotion(size_t ci, size_t cj, size_t ni, size_t nj, Game *game) : QFrame(game)
{
    cur_pos_i = ci;
    cur_pos_j = cj;
    new_pos_i = ni;
    new_pos_j = nj;
    setStyleSheet(".QLabel  { background-color : rgb(255,255,255);}");
    QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect();
    effect->setColor(Qt::black);
    effect->setBlurRadius(10);
    effect->setXOffset(1);
    effect->setYOffset(1);
    setGraphicsEffect(effect);
    char c = game->current_player;
    queen = new Piece(false, c, 'q',this);
    rook = new Piece(false, c, 'r',this);
    knight = new Piece(false, c, 'n',this);
    bishop = new Piece(false, c, 'b',this);
    queen->setGeometry(0,0,100,100);
    rook->setGeometry(0,100,100,100);
    knight->setGeometry(0,200,100,100);
    bishop->setGeometry(0,300,100,100);
}

void PawnPromotion::mousePressEvent(QMouseEvent *event)
{
    Game *game = static_cast<Game *>(parent());
    char c = 'w';
    if (game->current_player == 'w') {
        c = 'b';
    }
    bool take = false;
    if (!game->board_pieces[new_pos_i][new_pos_j]->empty) {
        take = true;
    }
    int p = event->pos().ry();
    if (p < 100) {
        game->board_pieces[new_pos_i][new_pos_j]->setAs(new Piece(false,c,'q',game));
    }
    else if (p < 200) {
        game->board_pieces[new_pos_i][new_pos_j]->setAs(new Piece(false,c,'r',game));
    }
    else if (p < 300) {
        game->board_pieces[new_pos_i][new_pos_j]->setAs(new Piece(false,c,'n',game));
    }
    else {
        game->board_pieces[new_pos_i][new_pos_j]->setAs(new Piece(false,c,'b',game));
    }
    TestPiece piece(game->board_pieces[cur_pos_i][cur_pos_j]->empty, game->board_pieces[cur_pos_i][cur_pos_j]->colour, game->board_pieces[cur_pos_i][cur_pos_j]->type);
    game->board_pieces[cur_pos_i][cur_pos_j]->setEmpty();
    std::vector<std::vector<TestPiece>> new_board;
    for (auto r : game->board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        new_board.push_back(temp);
    }
    game->test_board = new_board;

    QString S = moveToString(*game, piece, QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), QPoint(int(new_pos_j*100),int(new_pos_i*100)), take);
    S += "=";
    S += QString(game->board_pieces[new_pos_i][new_pos_j]->type).toUpper();
    QPoint *king_pos;
    if (game->current_player == 'w') {
        king_pos = game->white_king_pos;
    }
    else {
        king_pos = game->black_king_pos;
    }
    if (isCheckmate(*game)) {
        S += "#";
    }
    else if (isKingInCheck(game->test_board, game->current_player, king_pos)) {
        S += "+";
    }
    game->moveListSignal(S, game->turn_no - 1);
    //adds move to list of moves
    game->turn_list.push_back(TurnInfo(piece,new QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), new QPoint(int(new_pos_j*100),int(new_pos_i*100)), game->test_board, S, game->black_kingside, game->black_queenside, game->white_kingside, game->white_queenside));
    //check the promotion didnt cause the game to end
    isGameOver(*game);
    game->normal = true;
    this->close();
}
