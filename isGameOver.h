#ifndef ISGAMEOVER_H
#define ISGAMEOVER_H

#include "game.h"

bool isCheckmate(const Game &game);
void isGameOver(Game &game);

#endif // ISGAME_OVER_H
