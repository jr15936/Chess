#include "sidebar.h"
#include "game.h"
#include <QLabel>
#include <iostream>
#include <QGraphicsDropShadowEffect>
#include <QScrollArea>
#include <QTextEdit>
#include <QTextCursor>

Sidebar::Sidebar(Game *parent) : QLabel(parent)
{
    setStyleSheet("QLabel { background-color : rgb(255,255,255);}");
    QLabel *top_bar = new QLabel(this);
    QLabel *bot_bar = new QLabel(this);
    top_bar->setGeometry(0,0,256,70);
    bot_bar->setGeometry(0,730,256,70);
    top_bar->setStyleSheet("QLabel { background-color : rgb(220,220,220); font-size : 16px; color : rgb(60,60,60); font-family : Arial;}");
    bot_bar->setStyleSheet("QLabel { background-color : rgb(220,220,220); font-size : 16px; color : rgb(60,60,60); font-family : Arial;}");
    top_bar->setText("Black");
    bot_bar->setText("White");
    top_bar->setAlignment(Qt::AlignTop);
    bot_bar->setAlignment(Qt::AlignTop);
    top_bar->setMargin(10);
    bot_bar->setMargin(10);

    move_list = new QTextEdit(this);
    move_list->setStyleSheet("QTextEdit {font-family : Consolas;}");
    move_list->setGeometry(4,76,248,586);
    move_list->setReadOnly(true);

    difference = 0;
    difference_label = new QLabel(this);
    difference_label->setStyleSheet("QLabel { background-color : rgb(220,220,220); font-size : 16px; color : rgb(100,100,100); font-family : Arial;}");
    difference_label->setGeometry(-1000,-1000,32,32);
    difference_label->show();
    reset_game = new QPushButton(parent);
    backward = new QPushButton(parent);
    forward = new QPushButton(parent);
    hint = new QPushButton(parent);
    reset_game->setGeometry(860,670,50,50);
    hint->setGeometry(920,670,50,50);
    backward->setGeometry(980,670,50,50);
    forward->setGeometry(1040,670,50,50);
    QObject::connect(reset_game, SIGNAL( released()), parent, SLOT (resetGame()));
    QObject::connect(hint, SIGNAL( released()), parent, SLOT (showHint()));
    QObject::connect(backward, SIGNAL( released()), parent, SLOT (prevTurn()));
    QObject::connect(forward, SIGNAL( released()), parent, SLOT (nextTurn()));
    QObject::connect(parent, SIGNAL( updateTakenPieces(TestPiece)), this, SLOT(performUpdate(TestPiece)));
    QObject::connect(parent, SIGNAL( resetSidebarSignal()), this, SLOT (resetSidebar()));
    QObject::connect(parent, SIGNAL( prevSidebarSignal(char)), this, SLOT( prevSidebar(char)));
    QObject::connect(parent, SIGNAL( nextSidebarSignal(char)), this, SLOT( nextSidebar(char)));
    QObject::connect(parent, SIGNAL( moveListSignal(QString, int)), this, SLOT( addMoveToMoveList(QString, int)));
}

void Sidebar::addMoveToMoveList(QString moveString, int turn_no)
{
    if (moveString == QString("")) {
        if (turn_no == -1) {
            move_list->moveCursor(QTextCursor::End);
            QTextCursor c = move_list->textCursor();
            c.select(QTextCursor::BlockUnderCursor);
            c.deleteChar();
        }
        else {
            move_list->moveCursor(QTextCursor::End);
            QTextCursor c = move_list->textCursor();
            int pos = c.position();
            c.setPosition(pos-3,QTextCursor::KeepAnchor);
            QString S = c.selectedText();
            int i = 3;
            while (QString(S[0]).toStdString() != " ") {
                c.setPosition(pos-i,QTextCursor::KeepAnchor);
                S = c.selectedText();
                ++i;
            }
            c.setPosition(pos+1-i,QTextCursor::KeepAnchor);
            c.deleteChar();
        }
        return;
    }

    if (turn_no%2 == 0) {

        QString S = QString::number(turn_no/2 + 1) + QString(". ") + moveString;
        int spaces = 10 - moveString.length();
        for (int i = 0; i < spaces; ++i) {
            S += QString(" ");
        }
        move_list->append(S);
    }
    else {
        move_list->moveCursor(QTextCursor::End);
        move_list->insertPlainText(moveString);
        move_list->moveCursor(QTextCursor::End);
    }
}

void Sidebar::updateDifferenceLabel(char colour, char type)
{
    int t = 0;
    size_t l = 0;
    switch (type) {
    case 'p':
        t = 1;
        break;
    case 'r':
        t = 5;
        break;
    case 'q':
        t = 8;
        break;
    default:
        t = 3;
        break;
    }
    if (colour == 'w') {
        difference += t;
    }
    else {
        difference -= t;
    }
    if (difference < 0) {
        l = captured_by_white.size();
        difference_label->setGeometry(int(l*16 + 16),760,32,32);
        difference_label->setText("+" + QString::number(-difference));
        difference_label->show();
    }
    else if (difference > 0) {
        l = captured_by_black.size();
        difference_label->setGeometry(int(l*16 + 16),30,32,32);
        difference_label->setText("+" + QString::number(difference));
        difference_label->show();
    }
    else {
        difference_label->setGeometry(-1000,-1000,32,32);
        difference_label->setText("+" + QString::number(difference));
        difference_label->show();
    }
}

void Sidebar::prevSidebar(char colour)
{
    if (colour == 'b') {
        Piece *p = *(captured_by_white.end() - 1);
        cbw_history.push_back(p);
        captured_by_white.pop_back();
        p->setGeometry(-1000,-1000,32,32);
        updateDifferenceLabel('w', p->type);
    }

    else {
        Piece *p = *(captured_by_black.end() - 1);
        cbb_history.push_back(p);
        captured_by_black.pop_back();
        p->setGeometry(-1000,-1000,32,32);
        updateDifferenceLabel('b', p->type);
    }

}

void Sidebar::nextSidebar(char colour)
{
    if (colour == 'w') {
        Piece *p = *(cbw_history.end() - 1);
        captured_by_white.push_back(p);
        cbw_history.pop_back();
        size_t l = captured_by_white.size();
        p->setGeometry(int((l - 1)*16),760,32,32);
        updateDifferenceLabel('b', p->type);
    }
    else {
        Piece *p = *(cbb_history.end() - 1);
        captured_by_black.push_back(p);
        cbb_history.pop_back();
        size_t l = captured_by_black.size();
        p->setGeometry(int((l - 1)*16),30,32,32);
        updateDifferenceLabel('w', p->type);
    }
}

void Sidebar::resetSidebar()
{
    for (auto pt : captured_by_white) {
        pt->close();
    }
    for (auto pt : captured_by_black) {
        pt->close();
    }
    captured_by_white = std::vector<Piece *>();
    captured_by_black = std::vector<Piece *>();
    difference = 0;
    difference_label->setGeometry(-1000,-1000,32,32);
    move_list->clear();
}

void Sidebar::performUpdate(TestPiece test_piece)
{

    size_t l = 0;
    if (test_piece.colour == 'b') {
        Piece *piece = new Piece(false,'b',test_piece.type,this);
        captured_by_white.push_back(piece);
        piece->setStyleSheet("QLabel { background-color : rgba(0,0,0,0); }");
        l = captured_by_white.size();
        piece->setGeometry(int((l - 1)*16),760,32,32);
        piece->setPixmap((*piece->pixmap()).scaled(piece->width(),piece->height(),Qt::KeepAspectRatio));
        piece->show();
    }
    else {
        Piece *piece = new Piece(false,'w',test_piece.type,this);
        captured_by_black.push_back(piece);
        piece->setStyleSheet("QLabel { background-color : rgba(0,0,0,0); }");
        l = captured_by_black.size();
        piece->setGeometry(int((l - 1)*16),30,32,32);
        piece->setPixmap((*piece->pixmap()).scaled(piece->width(),piece->height(),Qt::KeepAspectRatio));
        piece->show();
    }

    updateDifferenceLabel(test_piece.colour, test_piece.type);

}
