#ifndef ISPATHEMPTY_H
#define ISPATHEMPTY_H

#include <QFrame>
#include "piece.h"

//returns true if a pieces path is not blocked, false otherwise
bool isPathEmpty(std::vector<std::vector<TestPiece>> board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j);

#endif // ISPATHEMPTY_H
