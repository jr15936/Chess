#include "validMove.h"
#include "testMove.h"
#include "game.h"
#include "isAttacked.h"
#include <iostream>

bool isKingInCheck(std::vector<std::vector<TestPiece>> board, char colour, QPoint *king_pos)
{
    if (colour == 'w') {
        return isAttacked(board, 'w', king_pos);
    }
    else {
        return isAttacked(board, 'b', king_pos);
    }
}

bool PawnMoves(const Game &game, TestPiece piece, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (cur_pos_j != new_pos_j && cur_pos_j - 1 != new_pos_j && cur_pos_j + 1 != new_pos_j) {
        return false;
    }
    if (piece.colour == 'w') {
        if (cur_pos_i - 1 == new_pos_i) {
            if (cur_pos_j == new_pos_j) {
                return game.test_board[new_pos_i][new_pos_j].empty;
            }
            else if (cur_pos_j - 1 == new_pos_j || cur_pos_j + 1 == new_pos_j) {
                if (game.test_board[new_pos_i][new_pos_j].colour == 'b') {
                    return true;
                }
                //en passant valid move
                else if (game.test_board[new_pos_i][new_pos_j].empty && cur_pos_i == 3) {
                    //checks that the pawn being taken moved in the last turn
                    TurnInfo temp_turn = game.turn_list[size_t(game.turn_no)];
                    if (temp_turn.piece.type == 'p' && size_t(temp_turn.cur_pos->ry()/100) == 1 && size_t(temp_turn.new_pos->ry()/100) == 3 && size_t(temp_turn.new_pos->rx()/100) == new_pos_j) {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
        }
        //double move only on first move for that pawn
        else if (cur_pos_i == 6 && new_pos_i == 4 && cur_pos_j == new_pos_j) {
            return game.test_board[new_pos_i][new_pos_j].empty;
        }
        else {
            return false;
        }
    }
    else {
        if (cur_pos_i + 1 == new_pos_i) {
            if (cur_pos_j == new_pos_j) {
                return game.test_board[new_pos_i][new_pos_j].empty;
            }
            else if (cur_pos_j - 1 == new_pos_j || cur_pos_j + 1 == new_pos_j) {
                if (game.test_board[new_pos_i][new_pos_j].colour == 'w') {
                    return true;
                }
                //en passant valid move
                else if (game.test_board[new_pos_i][new_pos_j].empty && cur_pos_i == 4) {
                    //checks that the pawn being taken moved in the last turn
                    TurnInfo temp_turn = game.turn_list[size_t(game.turn_no)];
                    if (temp_turn.piece.type == 'p' && size_t(temp_turn.cur_pos->ry()/100) == 6 && size_t(temp_turn.new_pos->ry()/100) == 4 && size_t(temp_turn.new_pos->rx()/100) == new_pos_j) {
                        return true;
                    }
                }
                else {
                    return false;
                }
            }
        }
        //double move only on first move for that pawn
        else if (cur_pos_i == 1 && new_pos_i == 3 && cur_pos_j == new_pos_j) {
            return game.test_board[new_pos_i][new_pos_j].empty;
        }
        else {
            return false;
        }
    }
    return false;
}

bool rookMoves(const Game &game, TestPiece piece, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (new_pos_i == cur_pos_i || new_pos_j == cur_pos_j) {
        if (piece.colour == 'w' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'b')) {
            return true;
        }
        else if (piece.colour == 'b' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'w')) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

bool knightMoves(const Game &game, TestPiece piece, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if ((new_pos_i == cur_pos_i - 2 && new_pos_j == cur_pos_j + 1) ||
        (new_pos_i == cur_pos_i - 2 && new_pos_j == cur_pos_j - 1) ||
        (new_pos_i == cur_pos_i + 2 && new_pos_j == cur_pos_j + 1) ||
        (new_pos_i == cur_pos_i + 2 && new_pos_j == cur_pos_j - 1) ||
        (new_pos_i == cur_pos_i - 1 && new_pos_j == cur_pos_j + 2) ||
        (new_pos_i == cur_pos_i - 1 && new_pos_j == cur_pos_j - 2) ||
        (new_pos_i == cur_pos_i + 1 && new_pos_j == cur_pos_j + 2) ||
        (new_pos_i == cur_pos_i + 1 && new_pos_j == cur_pos_j - 2) ) {
        if (piece.colour == 'w' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'b')) {
            return true;
        }
        else if (piece.colour == 'b' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'w')) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

bool bishopMoves(const Game &game, TestPiece piece, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (new_pos_i - cur_pos_i == new_pos_j - cur_pos_j || new_pos_i - cur_pos_i == cur_pos_j - new_pos_j) {
        if (piece.colour == 'w' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'b')) {
            return true;
        }
        else if (piece.colour == 'b' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'w')) {
            return true;
        }
        else {
            return false;
        }
    }
    else {
        return false;
    }
}

bool kingMoves(const Game &game, TestPiece piece, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if ((new_pos_i == cur_pos_i - 1 && new_pos_j == cur_pos_j - 1) ||
        (new_pos_i == cur_pos_i - 1 && new_pos_j == cur_pos_j + 1) ||
        (new_pos_i == cur_pos_i - 1 && new_pos_j == cur_pos_j    ) ||
        (new_pos_i == cur_pos_i + 1 && new_pos_j == cur_pos_j - 1) ||
        (new_pos_i == cur_pos_i + 1 && new_pos_j == cur_pos_j + 1) ||
        (new_pos_i == cur_pos_i + 1 && new_pos_j == cur_pos_j    ) ||
        (new_pos_i == cur_pos_i     && new_pos_j == cur_pos_j - 1) ||
        (new_pos_i == cur_pos_i     && new_pos_j == cur_pos_j + 1) ) {
        if (piece.colour == 'w' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'b')) {
            return true;
        }
        else if (piece.colour == 'b' && (game.test_board[new_pos_i][new_pos_j].empty || game.test_board[new_pos_i][new_pos_j].colour == 'w')) {
            return true;
        }
        else {
            return false;
        }
    }

    else if (piece.colour == 'w') {
        if (cur_pos_i == 7 && cur_pos_j == 4) {
            if (game.white_kingside && game.test_board[7][5].empty && game.test_board[7][6].empty && !isAttacked(game.test_board, 'w', new QPoint(500,700))) {
                return true;
            }
            else if (game.white_queenside && game.test_board[7][3].empty && game.test_board[7][2].empty && game.test_board[7][1].empty && !isAttacked(game.test_board, 'w', new QPoint(300,700))) {
                return true;
            }
        }
        else {
            return false;
        }
    }

    else if (piece.colour == 'b') {
        if (cur_pos_i == 0 && cur_pos_j == 4) {
            if (game.black_kingside && game.test_board[0][5].empty && game.test_board[0][6].empty && !isAttacked(game.test_board, 'b', new QPoint(500,0))) {
                return true;
            }
            else if (game.black_queenside && game.test_board[0][3].empty && game.test_board[0][2].empty && game.test_board[0][1].empty && !isAttacked(game.test_board, 'b', new QPoint(300,0))) {
                return true;
            }
        }
        else {
            return false;
        }
    }

    else {
        return false;
    }
    return false;
}

bool validMove(const Game &game, QPoint *cur_pos, QPoint *new_pos)
{
    size_t cur_pos_i = size_t((cur_pos->ry())/100);
    size_t cur_pos_j = size_t((cur_pos->rx())/100);
    size_t new_pos_i = size_t((new_pos->ry())/100);
    size_t new_pos_j = size_t((new_pos->rx())/100);
    TestPiece piece = TestPiece(game.board_pieces[cur_pos_i][cur_pos_j]->empty, game.board_pieces[cur_pos_i][cur_pos_j]->colour, game.board_pieces[cur_pos_i][cur_pos_j]->type);

    if (piece.colour != game.current_player) {
        return false;
    }
    std::vector<std::vector<TestPiece>> new_test_board = game.test_board;
    testMove(new_test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);

    QPoint *king_pos = new QPoint(-1,-1);
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (new_test_board[i][j].colour == piece.colour && new_test_board[i][j].type == 'k') {
                *king_pos = QPoint(int(j*100), int(i*100));
            }
        }
    }
    if (piece.colour == 'w') {
        if (isKingInCheck(new_test_board, 'w', king_pos)) {
            return false;
        }
    }
    else {
        if (isKingInCheck(new_test_board, 'b', king_pos)) {
            return false;
        }
    }

    switch (piece.type) {
    case 'p':
        return (PawnMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j) && isPathEmpty(game.test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j));
    case 'r':
        return (rookMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j) && isPathEmpty(game.test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j));
    case 'n':
        return knightMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
    case 'b':
        return (bishopMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j) && isPathEmpty(game.test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j));
    case 'q':
        return ((rookMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j) || bishopMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j)) && isPathEmpty(game.test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j));
    case 'k':
        return (kingMoves(game, piece, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j) && isPathEmpty(game.test_board, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j));
    default:
        return false;
    }
}

std::vector<QPoint *> findValidMove(const Game &game)
{
    std::vector<QPoint *> points;
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (game.test_board[i][j].colour == game.current_player) {
                for (size_t x = 0; x < 8; ++x) {
                    for (size_t y = 0; y < 8; ++y) {
                        if (validMove(game, new QPoint(int(j*100), int(i*100)), new QPoint(int(y*100), int(x*100)))) {
                            points.push_back(new QPoint(int(j*100), int(i*100)));
                            points.push_back(new QPoint(int(y*100), int(x*100)));
                            return points;
                        }
                    }
                }
            }
        }
    }
    return points;
}
