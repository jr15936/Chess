#include "move.h"
#include "piece.h"
#include "game.h"
#include "pawnpromotion.h"
#include "validMove.h"
#include "algebraicnotation.h"
#include <iostream>

void castleKingside(Game &game, size_t cur_pos_i)
{
    game.board_pieces[cur_pos_i][6]->setAs(game.board_pieces[cur_pos_i][4]);
    game.board_pieces[cur_pos_i][5]->setAs(game.board_pieces[cur_pos_i][7]);
    game.board_pieces[cur_pos_i][4]->setEmpty();
    game.board_pieces[cur_pos_i][7]->setEmpty();
}

void castleQueenside(Game &game, size_t cur_pos_i)
{
    game.board_pieces[cur_pos_i][2]->setAs(game.board_pieces[cur_pos_i][4]);
    game.board_pieces[cur_pos_i][3]->setAs(game.board_pieces[cur_pos_i][0]);
    game.board_pieces[cur_pos_i][4]->setEmpty();
    game.board_pieces[cur_pos_i][0]->setEmpty();
}

void moveKing(Game &game, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    TestPiece piece(game.board_pieces[cur_pos_i][cur_pos_j]->empty, game.board_pieces[cur_pos_i][cur_pos_j]->colour, game.board_pieces[cur_pos_i][cur_pos_j]->type);
    bool take = false;
    if (!game.board_pieces[new_pos_i][new_pos_j]->empty) {
        take = true;
    }
    //Sets the castling bool to false if the king moves and updates the kings position
    if (game.board_pieces[cur_pos_i][cur_pos_j]->colour == 'w') {
        game.white_king_pos = new QPoint(int(new_pos_j*100),int(new_pos_i*100));
        game.white_kingside = false;
        game.white_queenside = false;
    }
    else if (game.board_pieces[cur_pos_i][cur_pos_j]->colour == 'b') {
        game.black_king_pos = new QPoint(int(new_pos_j*100),int(new_pos_i*100));
        game.black_kingside = false;
        game.black_queenside = false;
    }
    QString S = "";
    if (cur_pos_j == 4 && new_pos_j == 6) {
        castleKingside(game, cur_pos_i);
        S = QString("O-O");
    }
    else if (cur_pos_j == 4 && new_pos_j == 2) {
        castleQueenside(game, cur_pos_i);
        S = QString("O-O-O");
    }
    //Performs regular king move
    else {
        game.board_pieces[new_pos_i][new_pos_j]->setAs(game.board_pieces[cur_pos_i][cur_pos_j]);
        game.board_pieces[cur_pos_i][cur_pos_j]->setEmpty();
        S = moveToString(game, piece, QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), QPoint(int(new_pos_j*100),int(new_pos_i*100)), take);
    }

    std::vector<std::vector<TestPiece>> new_board;
    for (auto r : game.board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        new_board.push_back(temp);
    }
    game.test_board = new_board;
    //adds move to list of moves
    if (size_t(game.turn_no) < game.turn_list.size() - 1) {
        game.turn_list = std::vector<TurnInfo>(game.turn_list.begin(), game.turn_list.begin() + game.turn_no + 1);
    }
    game.moveListSignal(S, game.turn_no);
    game.turn_list.push_back(TurnInfo(piece,new QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), new QPoint(int(new_pos_j*100),int(new_pos_i*100)), game.test_board, S, game.black_kingside, game.black_queenside, game.white_kingside, game.white_queenside));
}

void promotePawn(Game &game, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    PawnPromotion *pro = new PawnPromotion(cur_pos_i, cur_pos_j, new_pos_i, new_pos_j, &game);
    if (game.current_player == 'w') {
        pro->setGeometry(int(new_pos_j*100),0,100,400);
    }
    else {
        pro->setGeometry(int(new_pos_j*100),400,100,400);
    }
    pro->show();
    game.normal = false;
}

void enPassant(Game &game, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    if (game.current_player == 'w') {
        game.updateTakenPieces(TestPiece(false,'b','p'));
    }
    else {
        game.updateTakenPieces(TestPiece(false,'w','p'));
    }
    game.board_pieces[new_pos_i][new_pos_j]->setAs(game.board_pieces[cur_pos_i][cur_pos_j]);
    game.board_pieces[cur_pos_i][cur_pos_j]->setEmpty();
    game.board_pieces[cur_pos_i][new_pos_j]->setEmpty();
}

void movePawn(Game &game, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    TestPiece piece(game.board_pieces[cur_pos_i][cur_pos_j]->empty, game.board_pieces[cur_pos_i][cur_pos_j]->colour, game.board_pieces[cur_pos_i][cur_pos_j]->type);
    game.fifty_move = 0;
    bool take = false;
    if (!game.board_pieces[new_pos_i][new_pos_j]->empty) {
        take = true;
    }
    if (cur_pos_j != new_pos_j && game.board_pieces[new_pos_i][new_pos_j]->empty) {
        enPassant(game, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
        take = true;
    }
    else if (((cur_pos_i == 1 && new_pos_i == 0) && game.board_pieces[cur_pos_i][cur_pos_j]->colour == 'w') ||
              (cur_pos_i == 6 && new_pos_i == 7 && game.board_pieces[cur_pos_i][cur_pos_j]->colour == 'b')) {
        promotePawn(game, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
        return;
    }
    else {
        game.board_pieces[new_pos_i][new_pos_j]->setAs(game.board_pieces[cur_pos_i][cur_pos_j]);
        game.board_pieces[cur_pos_i][cur_pos_j]->setEmpty();
    }

    std::vector<std::vector<TestPiece>> new_board;
    for (auto r : game.board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        new_board.push_back(temp);
    }
    game.test_board = new_board;

    //adds move to list of moves
    if (size_t(game.turn_no) < game.turn_list.size() - 1) {
        game.turn_list = std::vector<TurnInfo>(game.turn_list.begin(), game.turn_list.begin() + game.turn_no + 1);
    }
    QString S = moveToString(game, piece, QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), QPoint(int(new_pos_j*100),int(new_pos_i*100)), take);
    game.moveListSignal(S, game.turn_no);
    game.turn_list.push_back(TurnInfo(piece,new QPoint(int(cur_pos_j*100),int(cur_pos_i*100)), new QPoint(int(new_pos_j*100),int(new_pos_i*100)), game.test_board, S, game.black_kingside, game.black_queenside, game.white_kingside, game.white_queenside));
}

void performMove(Game &game, QPoint *cur_pos, QPoint *new_pos)
{
    size_t cur_pos_j = size_t((cur_pos->rx())/100);
    size_t cur_pos_i = size_t((cur_pos->ry())/100);
    size_t new_pos_j = size_t((new_pos->rx())/100);
    size_t new_pos_i = size_t((new_pos->ry())/100);
    TestPiece piece(game.board_pieces[cur_pos_i][cur_pos_j]->empty, game.board_pieces[cur_pos_i][cur_pos_j]->colour, game.board_pieces[cur_pos_i][cur_pos_j]->type);
    TestPiece new_piece(game.board_pieces[new_pos_i][new_pos_j]->empty, game.board_pieces[new_pos_i][new_pos_j]->colour, game.board_pieces[new_pos_i][new_pos_j]->type);
    QString S = "";

    if ((piece.colour == 'b' && new_piece.colour == 'w') || (piece.colour == 'w' && new_piece.colour == 'b')) {
        game.updateTakenPieces(new_piece);
        S = moveToString(game, piece, *cur_pos, *new_pos, true);
    }
    else {
        S = moveToString(game, piece, *cur_pos, *new_pos, false);
    }

    if (!game.board_pieces[new_pos_i][new_pos_j]->empty) {
        game.fifty_move = 0;
    }

    //deals with special king move: castling and also updates kings position
    if (piece.type == 'k') {
        moveKing(game, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
        return;
    }
    //deals with special pawn moves: en passant and promotion
    else if (piece.type == 'p') {
        movePawn(game, cur_pos_i, cur_pos_j, new_pos_i, new_pos_j);
        return;
    }
    else {
        if (piece.type == 'r') {
            if (piece.colour == 'w') {
                if (cur_pos_i == 7 && cur_pos_j == 7) {
                    game.white_kingside = false;
                }
                else if (cur_pos_i == 7 && cur_pos_j == 0) {
                    game.white_queenside = false;
                }
            }
            else {
                if (cur_pos_i == 0 && cur_pos_j == 7) {
                    game.black_kingside = false;
                }
                else if (cur_pos_i == 0 && cur_pos_j == 0) {
                    game.black_queenside = false;
                }
            }
        }
        game.board_pieces[new_pos_i][new_pos_j]->setAs(game.board_pieces[cur_pos_i][cur_pos_j]);
        game.board_pieces[cur_pos_i][cur_pos_j]->setEmpty();
    }

    std::vector<std::vector<TestPiece>> new_board;
    for (auto r : game.board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        new_board.push_back(temp);
    }
    game.test_board = new_board;

    //adds move to list of moves
    if (size_t(game.turn_no) < game.turn_list.size() - 1) {
        game.turn_list = std::vector<TurnInfo>(game.turn_list.begin(), game.turn_list.begin() + game.turn_no + 1);
    }
    game.moveListSignal(S, game.turn_no);
    game.turn_list.push_back(TurnInfo(piece,new QPoint(*cur_pos), new QPoint(*new_pos), game.test_board, S, game.black_kingside, game.black_queenside, game.white_kingside, game.white_queenside));
}
