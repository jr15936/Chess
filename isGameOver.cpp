#include "game.h"
#include "piece.h"
#include "validMove.h"
#include "isGameOver.h"
#include <QFrame>
#include <iostream>
#include <QGraphicsDropShadowEffect>
#include <QPushButton>

//returns true if the current_player has an available move
bool ableToMove(const Game &game)
{
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (game.test_board[i][j].colour == game.current_player) {
                for (size_t x = 0; x < 8; ++x) {
                    for (size_t y = 0; y < 8; ++y) {
                        if (validMove(game, new QPoint(int(j*100), int(i*100)), new QPoint(int(y*100), int(x*100)))) {
                            return true;
                        }
                    }
                }
            }
        }
    }
    return false;
}

bool drawByStalemate(const Game &game)
{
    if (!ableToMove(game)) {
        if (game.current_player == 'w' && !isKingInCheck(game.test_board, 'w', game.white_king_pos)) {
            return true;
        }
        else if (game.current_player == 'b' && !isKingInCheck(game.test_board, 'b', game.black_king_pos)) {
            return true;
        }
    }
    return false;
}

//TODO: en passant
bool drawByRepition(const Game &game)
{
    if (game.turn_no < 9) {
        return false;
    }
    int t = 0;
    std::vector<std::vector<bool>> cb_list;
    std::vector<std::vector<TestPiece>> cur_board = (*(game.turn_list.end() - 1)).board_pieces;
    for (auto i = game.turn_list.begin(); i != game.turn_list.end(); ++i) {
        if ((*i).board_pieces == cur_board) {
            ++t;
            cb_list.push_back((*i).castle_bools);
        }
    }
    if (t > 2 && equal(cb_list.begin() + 1, cb_list.end(), cb_list.begin())) {
        return true;
    }
    else {
        return false;
    }
}

bool drawByFiftyMove(const Game &game)
{
    if (game.fifty_move >= 99) {
        return true;
    }
    else {
        return false;
    }
}

bool isCheckmate(const Game &game)
{
    QPoint *king_pos = new QPoint(-1,-1);
    if (game.current_player == 'w') {
        king_pos = game.white_king_pos;
    }
    else {
        king_pos = game.black_king_pos;
    }
    if (!ableToMove(game) && isKingInCheck(game.test_board, game.current_player, king_pos)) {
        return true;
    }
    else {
        return false;
    }
}

bool insufficientMaterial(const Game &game)
{
    std::vector<TestPiece> white_pieces, black_pieces;
    for (auto r : game.test_board) {
        for (TestPiece p : r) {
            if (p.type == 'p' || p.type == 'q' || p.type == 'r') {
                return false;
            }
            if (p.colour == 'w' && p.type != 'k') {
                white_pieces.push_back(p);
            }
            else if (p.colour == 'b' && p.type != 'k') {
                black_pieces.push_back(p);
            }
        }
    }

    if (white_pieces.size() == 0 && black_pieces.size() == 0) {
        return true;
    }
    else if (white_pieces.size() == 0 && black_pieces.size() == 1) {
        return true;
    }
    else if (black_pieces.size() == 0 && white_pieces.size() == 1) {
        return true;
    }
    else if (black_pieces.size() == 0 && white_pieces.size() > 1) {
        if (any_of(white_pieces.begin(), white_pieces.end(), [](TestPiece p){return p.type == 'n';})) {
            return false;
        }
        else if (all_of(white_pieces.begin(), white_pieces.end(), [](TestPiece p){return p.type == 'b';})) {
            std::vector<int> square_colours;
            for (size_t i = 0; i < 8; ++i) {
                for (size_t j = 0; j < 8; ++j) {
                    if (game.test_board[i][j].type == 'b') {
                        square_colours.push_back((i-j)%2);
                    }
                }
            }
            if (equal(square_colours.begin() + 1, square_colours.end(), square_colours.begin())) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else if (white_pieces.size() == 0 && black_pieces.size() > 1) {
        if (any_of(black_pieces.begin(), black_pieces.end(), [](TestPiece p){return p.type == 'n';})) {
            return false;
        }
        else if (all_of(black_pieces.begin(), black_pieces.end(), [](TestPiece p){return p.type == 'b';})) {
            std::vector<int> square_colours;
            for (size_t i = 0; i < 8; ++i) {
                for (size_t j = 0; j < 8; ++j) {
                    if (game.test_board[i][j].type == 'b') {
                        square_colours.push_back((i-j)%2);
                    }
                }
            }
            if (equal(square_colours.begin() + 1, square_colours.end(), square_colours.begin())) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    else if (black_pieces.size() > 1 && white_pieces.size() > 1) {
        if (any_of(black_pieces.begin(), black_pieces.end(), [](TestPiece p){return p.type == 'n';})) {
            return false;
        }
        else if (any_of(white_pieces.begin(), white_pieces.end(), [](TestPiece p){return p.type == 'n';})) {
            return false;
        }
        std::vector<int> ws_colours, bs_colours;
        for (size_t i = 0; i < 8; ++i) {
            for (size_t j = 0; j < 8; ++j) {
                if (game.test_board[i][j].type == 'b') {
                    if (game.test_board[i][j].colour == 'w') {
                        ws_colours.push_back((i-j)%2);
                    }
                    else {
                        bs_colours.push_back((i-j)%2);
                    }
                }
            }
        }
        if (equal(ws_colours.begin() + 1, ws_colours.end(), ws_colours.begin())) {
            if (equal(bs_colours.begin() + 1, bs_colours.end(), bs_colours.begin())) {
                return (bs_colours[0] == ws_colours[0]);
            }
            else {
                return false;
            }
        }
    }
    return false;
}

void displayEndGame(Game &game, QString winning_text)
{
    game.end_msg = new QLabel(&game);
    QLabel *text = new QLabel(game.end_msg);
    text->setText(winning_text);
    text->setAlignment(Qt::AlignCenter);
    text->setGeometry(20,55,260,50);
    QGraphicsDropShadowEffect* effect = new QGraphicsDropShadowEffect();
    effect->setColor(Qt::black);
    effect->setBlurRadius(10);
    effect->setXOffset(1);
    effect->setYOffset(1);
    QPushButton *new_game = new QPushButton(game.end_msg);
    QObject::connect(new_game, SIGNAL (released()), &game, SLOT (resetGame()));
    new_game->setGeometry(20,130,260,50);
    new_game->setText("New Game");
    new_game->setStyleSheet("QPushButton { background-color : rgb(230,210,85);}");
    game.end_msg->setGraphicsEffect(effect);
    game.end_msg->setGeometry(250,150,300,200);
    game.end_msg->setStyleSheet("QLabel { background-color : White; font : 16px;}");
    game.end_msg->show();
}

void isGameOver(Game &game)
{
    QString winning_text;
    if (drawByStalemate(game)) {
        winning_text = "Draw by stalemate!\n";
    }
    else if (drawByRepition(game)) {
        winning_text = "Draw by threefold repition!\n";
    }
    else if (isCheckmate(game)) {
        if (game.current_player == 'b') {
            winning_text = "Checkmate! White wins.\n";
        }
        else {
            winning_text = "Checkmate! Black wins.\n";
        }
    }
    else if (drawByFiftyMove(game)) {
        winning_text = "Draw by fifty-move rule!\n";
    }
    else if (insufficientMaterial(game)) {
        winning_text = "Draw by insufficient material!\n";
    }
    else {
        return;
    }
    displayEndGame(game, winning_text);
}
