#ifndef PAWNPROMOTION_H
#define PAWNPROMOTION_H

#include <QFrame>
#include <QLabel>
#include "game.h"
#include "piece.h"

class PawnPromotion : public QFrame
{
    Q_OBJECT

public:

    size_t cur_pos_i;
    size_t cur_pos_j;
    size_t new_pos_i;
    size_t new_pos_j;
    Piece *queen;
    Piece *rook;
    Piece *knight;
    Piece *bishop;

    explicit PawnPromotion(size_t ci, size_t cj, size_t ni, size_t nj, Game *parent = nullptr);

protected:

    void mousePressEvent(QMouseEvent *event) override;

};


#endif // PAWNPROMOTION_H
