#include "algebraicnotation.h"
#include "game.h"
#include "validMove.h"
#include <QString>

QString positionToString(QPoint pos)
{
    int x = pos.rx()/100;
    int y = pos.ry()/100;
    QString S = "";
    switch (x) {
    case 0:
        S += 'a';
        break;
    case 1:
        S += 'b';
        break;
    case 2:
        S += 'c';
        break;
    case 3:
        S += 'd';
        break;
    case 4:
        S += 'e';
        break;
    case 5:
        S += 'f';
        break;
    case 6:
        S += 'g';
        break;
    case 7:
        S += 'h';
        break;
    }
    switch (y) {
    case 0:
        S += '8';
        break;
    case 1:
        S += '7';
        break;
    case 2:
        S += '6';
        break;
    case 3:
        S += '5';
        break;
    case 4:
        S += '4';
        break;
    case 5:
        S += '3';
        break;
    case 6:
        S += '2';
        break;
    case 7:
        S += '1';
        break;
    }
    return S;
}

QString moveToString(Game &game, TestPiece piece, QPoint cur_pos, QPoint new_pos, bool take)
{
    QString S = "";
    QString T = "";
    if (piece.type == 'p') {
        T += positionToString(new_pos);
        if (take) {
            S += positionToString(cur_pos).at(0);
            S += QString('x');
        }
        S += T;
    }
    else if (piece.type == 'k') {
        S += QString('K');
        if (take) {
            S += QString('x');
        }
        S += positionToString(new_pos);
    }
    else {
        S += QString(piece.type).toUpper();
        std::vector<std::vector<size_t>> possible;
        for (size_t i = 0; i < 8; ++i) {
            for (size_t j = 0; j < 8; ++j) {
                if (game.test_board[i][j].colour == piece.colour && game.test_board[i][j].type == piece.type) {
                    possible.push_back(std::vector<size_t>{i,j});
                }
            }
        }
        if (possible.size() == 1) {
            if (take) {
                S += QString('x');
            }
            S += positionToString(new_pos);
        }
        else {
            int no_of_possible = 0;
            for (auto pos : possible) {
                if (validMove(game, new QPoint(int(pos[1]*100), int(pos[0]*100)), &new_pos)) {
                    no_of_possible += 1;
                }
            }
            if (no_of_possible == 1) {
                if (take) {
                    S += QString('x');
                }
                S += positionToString(new_pos);
            }
            else {
                int no_of_x = 0;
                int no_of_y = 0;
                for (auto pos : possible) {
                    if (pos[0] == size_t(cur_pos.ry()/100)) {
                        no_of_x += 1;
                    }
                    if (pos[1] == size_t(cur_pos.rx()/100)) {
                        no_of_y += 1;
                    }
                }
                if (no_of_y > 1) {
                    S += positionToString(cur_pos).at(1);
                }
                else {
                    S += positionToString(cur_pos).at(0);
                }
                if (take) {
                    S += QString('x');
                }
                S += positionToString(new_pos);

            }
        }
    }
    return S;
}
