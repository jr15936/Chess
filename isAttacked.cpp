#include "isPathEmpty.h"
#include "piece.h"
#include <QFrame>

bool isPawnAttack(std::vector<std::vector<TestPiece>> board, char col, int pos_i, int pos_j)
{
    std::vector<std::vector<int>> positions;
    if (col == 'w') {
        positions.push_back(std::vector<int> {pos_i - 1, pos_j + 1});
        positions.push_back(std::vector<int> {pos_i - 1, pos_j - 1});
    }
    else {
        positions.push_back(std::vector<int> {pos_i + 1, pos_j + 1});
        positions.push_back(std::vector<int> {pos_i + 1, pos_j - 1});
    }

    for (auto p : positions) {
        if (p[0] <= 7 && p[0] >= 0 && p[1] <= 7 && p[1] >= 0) {
            size_t i = size_t(p[0]), j = size_t(p[1]);
            if (board[i][j].type == 'p' && board[i][j].colour != col) {
                return true;
            }
        }
    }
    return false;
}

bool isKnightAttack(std::vector<std::vector<TestPiece>> board, char col, int pos_i, int pos_j)
{
    std::vector<std::vector<int>> positions;
    positions.push_back(std::vector<int> {pos_i + 1, pos_j - 2});
    positions.push_back(std::vector<int> {pos_i - 1, pos_j + 2});
    positions.push_back(std::vector<int> {pos_i - 1, pos_j - 2});
    positions.push_back(std::vector<int> {pos_i + 2, pos_j + 1});
    positions.push_back(std::vector<int> {pos_i + 1, pos_j + 2});
    positions.push_back(std::vector<int> {pos_i + 2, pos_j - 1});
    positions.push_back(std::vector<int> {pos_i - 2, pos_j + 1});
    positions.push_back(std::vector<int> {pos_i - 2, pos_j - 1});

    for (auto p : positions) {
        if (p[0] <= 7 && p[0] >= 0 && p[1] <= 7 && p[1] >= 0) {
            size_t i = size_t(p[0]), j = size_t(p[1]);
            if (board[i][j].type == 'n' && board[i][j].colour != col) {
                return true;
            }
        }
    }
    return false;
}

bool isKingAttack(std::vector<std::vector<TestPiece>> board, char col, int pos_i, int pos_j)
{
    std::vector<std::vector<int>> positions;
    positions.push_back(std::vector<int> {pos_i + 1, pos_j + 1});
    positions.push_back(std::vector<int> {pos_i + 1, pos_j - 1});
    positions.push_back(std::vector<int> {pos_i + 1, pos_j    });
    positions.push_back(std::vector<int> {pos_i - 1, pos_j + 1});
    positions.push_back(std::vector<int> {pos_i - 1, pos_j - 1});
    positions.push_back(std::vector<int> {pos_i - 1, pos_j    });
    positions.push_back(std::vector<int> {pos_i    , pos_j + 1});
    positions.push_back(std::vector<int> {pos_i    , pos_j - 1});

    for (auto p : positions) {
        if (p[0] <= 7 && p[0] >= 0 && p[1] <= 7 && p[1] >= 0) {
            size_t i = size_t(p[0]), j = size_t(p[1]);
            if (board[i][j].type == 'k' && board[i][j].colour != col) {
                return true;
            }
        }
    }
    return false;
}

//also implements diagonal attacks from queens
bool isBishopAttack(std::vector<std::vector<TestPiece>> board, char col, int pos_i, int pos_j)
{
    std::vector<std::vector<int>> positions;
    for (int i = pos_i, j = pos_j; i < 8 && j < 8; ++i, ++j) {
        positions.push_back(std::vector<int> {i, j});
    }
    for (int i = pos_i, j = pos_j; i >= 0 && j < 8; --i, ++j) {
        positions.push_back(std::vector<int> {i, j});
    }
    for (int i = pos_i, j = pos_j; i < 8 && j >= 0; ++i, --j) {
        positions.push_back(std::vector<int> {i, j});
    }
    for (int i = pos_i, j = pos_j; i >= 0 && j >= 0; --i, --j) {
        positions.push_back(std::vector<int> {i, j});
    }

    for (auto p : positions) {
        size_t i = size_t(p[0]), j = size_t(p[1]);
        if ((board[i][j].type == 'b' || board[i][j].type == 'q') && board[i][j].colour != col) {
            if (isPathEmpty(board, i, j, size_t(pos_i), size_t(pos_j))) {
                return true;
            }
        }
    }
    return false;
}

//also implements h/v queen attacks
bool isRookAttack(std::vector<std::vector<TestPiece>> board, char col, int pos_i, int pos_j)
{
    std::vector<std::vector<int>> positions;
    for (int i = pos_i; i < 8; ++i) {
        positions.push_back(std::vector<int> {i, pos_j});
    }
    for (int i = pos_i; i >= 0; --i) {
        positions.push_back(std::vector<int> {i, pos_j});
    }
    for (int j = pos_j; j < 8; ++j) {
        positions.push_back(std::vector<int> {pos_i, j});
    }
    for (int j = pos_j; j >= 0; --j) {
        positions.push_back(std::vector<int> {pos_i, j});
    }

    for (auto p : positions) {
        size_t i = size_t(p[0]), j = size_t(p[1]);
        if ((board[i][j].type == 'r' || board[i][j].type == 'q') && board[i][j].colour != col) {
            if (isPathEmpty(board, i, j, size_t(pos_i), size_t(pos_j))) {
                return true;
            }
        }
    }
    return false;
}

bool isAttacked(std::vector<std::vector<TestPiece>> board, char col, QPoint *pos)
{
    int pos_i = int((pos->ry())/100);
    int pos_j = int((pos->rx())/100);
    if (isPawnAttack(board, col, pos_i, pos_j)) {
        return true;
    }
    else if (isKnightAttack(board, col, pos_i, pos_j)) {
        return true;
    }
    else if (isKingAttack(board, col, pos_i, pos_j)) {
        return true;
    }
    else if (isBishopAttack(board, col, pos_i, pos_j)) {
        return true;
    }
    else if (isRookAttack(board, col, pos_i, pos_j)) {
        return true;
    }
    else {
        return false;
    }
}
