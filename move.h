#ifndef MOVE_H
#define MOVE_H

#include "piece.h"
#include "game.h"

void performMove(Game &game, QPoint *cur_pos, QPoint *new_pos);

#endif // MOVE_H
