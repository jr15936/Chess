#ifndef VALIDMOVE_H
#define VALIDMOVE_H

#include "game.h"
#include <QFrame>

bool isKingInCheck(std::vector<std::vector<TestPiece>> board, char colour, QPoint *king_pos);
bool validMove(const Game &game, QPoint *cur_pos, QPoint *new_pos);
std::vector<QPoint *> findValidMove(const Game &game);

#endif // VALIDMOVE_H
