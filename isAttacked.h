#ifndef ISATTACKED_H
#define ISATTACKED_H

#include "piece.h"
#include "isPathEmpty.h"
#include <QFrame>

//returns true if a given position is under attack from an enemy piece
bool isAttacked(std::vector<std::vector<TestPiece>> board, char col, QPoint *pos);

#endif // ISATTACKED_H
