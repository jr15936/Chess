#ifndef TURNINFO_H
#define TURNINFO_H

#include <QFrame>
#include <piece.h>

class TurnInfo
{
public:
    TestPiece piece;
    QPoint *cur_pos;
    QPoint *new_pos;
    std::vector<std::vector<TestPiece>> board_pieces;
    QString moveText;
    std::vector<bool> castle_bools;

    TurnInfo() = default;
    TurnInfo(TestPiece p, QPoint *c, QPoint *n, std::vector<std::vector<TestPiece>> b, QString m, bool b_k, bool b_q, bool w_k, bool w_q) :
             piece(p), cur_pos(c), new_pos(n), board_pieces(b), moveText(m), castle_bools({b_k, b_q, w_k, w_q}) {}
};



#endif // TURNINFO_H
