#ifndef PIECE_H
#define PIECE_H

#include <QLabel>

class Piece : public QLabel
{

public:
    bool empty;
    char colour;
    char type;

    void setEmpty();
    void setAs(Piece *piece);

    explicit Piece(bool e = true, char c = '-', char t = '-', QWidget *parent = nullptr);

};

class TestPiece
{
public:
    bool empty;
    char colour;
    char type;

    void setEmpty();

    TestPiece() = default;
    TestPiece(bool e, char c, char t) : empty(e), colour(c), type(t) {}
};

bool operator==(const TestPiece &p1, const TestPiece &p2);

#endif // PIECE_H
