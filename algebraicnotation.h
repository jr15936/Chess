#ifndef ALGEBRAICNOTATION_H
#define ALGEBRAICNOTATION_H

#include "game.h"
#include <QString>

QString moveToString(Game &game, TestPiece piece, QPoint cur_pos, QPoint new_pos, bool take);

#endif // ALGEBRAICNOTATION_H
