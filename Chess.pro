TEMPLATE = app
TARGET = Chess


QT = core gui

greaterThan(QT_MAJOR_VERSION, 4):
    QT += widgets testlib

SOURCES += \
    main.cpp \
    piece.cpp \
    game.cpp \
    move.cpp \
    isPathEmpty.cpp \
    isGameOver.cpp \
    isAttacked.cpp \
    testMove.cpp \
    validMove.cpp \
    test.cpp \
    sidebar.cpp \
    pawnpromotion.cpp \
    algebraicnotation.cpp

RESOURCES += \
    resources.qrc

HEADERS += \
    piece.h \
    game.h \
    move.h \
    validMove.h \
    turnInfo.h \
    testMove.h \
    isPathEmpty.h \
    isAttacked.h \
    isGameOver.h \
    test.h \
    sidebar.h \
    pawnpromotion.h \
    algebraicnotation.h
