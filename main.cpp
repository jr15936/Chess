#include <QApplication>
#include <QLabel>
#include "game.h"
#include "sidebar.h"
#include "test.h"

//TODO LIST
/*
    1. fix move_list for check, checkmate
    2. adjust sizing and spacing on sidebar taken pieces graphic
*/


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QWidget mainWidget;
    mainWidget.setFixedSize(1200,1000);
    mainWidget.setStyleSheet(".QWidget  { background-color : rgb(100,100,100);}");

    QLabel *board_label = new QLabel(&mainWidget);
    board_label->setPixmap(QPixmap("://resources/chess_board.png"));
    board_label->setGeometry(50,100,800,800);

    Game *game = new Game(&mainWidget);
    game->setGeometry(50,100,1106,800);

    Sidebar *sidebar = new Sidebar(game);
    sidebar->setGeometry(850,0,256,800);

    mainWidget.setWindowTitle(QObject::tr("Chess"));
    mainWidget.show();

    //test(*game);

    return app.exec();
}
