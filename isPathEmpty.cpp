#include "isPathEmpty.h"
#include "game.h"
#include "piece.h"
#include <QFrame>

bool isPathEmpty(std::vector<std::vector<TestPiece>> board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j)
{
    //horizontal move case
    if (cur_pos_i == new_pos_i) {
        if (cur_pos_j < new_pos_j) {
            for (size_t t = cur_pos_j + 1; t < new_pos_j; ++t) {
                if (!board[cur_pos_i][t].empty) {
                    return false;
                }
            }
        }
        else {
            for (size_t t = new_pos_j + 1; t < cur_pos_j; ++t) {
                if (!board[cur_pos_i][t].empty) {
                    return false;
                }
            }
        }
        return true;
    }
    //vertical move case
    else if (cur_pos_j == new_pos_j) {
        if (cur_pos_i < new_pos_i) {
            for (size_t t = cur_pos_i + 1; t < new_pos_i; ++t) {
                if (!board[t][cur_pos_j].empty) {
                    return false;
                }
            }
        }
        else {
            for (size_t t = new_pos_i + 1; t < cur_pos_i; ++t) {
                if (!board[t][cur_pos_j].empty) {
                    return false;
                }
            }
        }
        return true;
    }
    //diagonal move case
    else {
        if (cur_pos_j < new_pos_j) {
            if (cur_pos_i < new_pos_i) {
                for (size_t t = cur_pos_j + 1, u = cur_pos_i + 1; t < new_pos_j && u < new_pos_i; ++t, ++u) {
                    if (!board[u][t].empty) {
                        return false;
                    }
                }
            }
            else {
                for (size_t t = cur_pos_j + 1, u = cur_pos_i - 1; t < new_pos_j && u > new_pos_i; ++t, --u) {
                    if (!board[u][t].empty) {
                        return false;
                    }
                }
            }
        }
        else {
            if (cur_pos_i < new_pos_i) {
                for (size_t t = cur_pos_j - 1, u = cur_pos_i + 1; t > new_pos_j && u < new_pos_i; --t, ++u) {
                    if (!board[u][t].empty) {
                        return false;
                    }
                }
            }
            else {
                for (size_t t = cur_pos_j - 1, u = cur_pos_i - 1; t > new_pos_j && u > new_pos_i; --t, --u) {
                    if (!board[u][t].empty) {
                        return false;
                    }
                }
            }
        }
        return true;
    }
}
