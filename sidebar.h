#ifndef SIDEBAR_H
#define SIDEBAR_H

#include "piece.h"
#include "game.h"
#include <QLabel>
#include <QPushButton>
#include <QTextEdit>

class Sidebar : public QLabel
{

    Q_OBJECT

public:

    std::vector<Piece *> captured_by_white;
    std::vector<Piece *> captured_by_black;
    std::vector<Piece *> cbw_history;
    std::vector<Piece *> cbb_history;
    QLabel *difference_label;
    int difference;

    QPushButton *reset_game;
    QPushButton *hint;
    QPushButton *backward;
    QPushButton *forward;

    QTextEdit *move_list;

    void updateDifferenceLabel(char colour, char type);

    explicit Sidebar(Game *parent = nullptr);

private slots:
    void addMoveToMoveList(QString moveString, int turn_no);
    void prevSidebar(char colour);
    void nextSidebar(char colour);
    void performUpdate(TestPiece piece);
    void resetSidebar();
};

#endif // SIDEBAR_H
