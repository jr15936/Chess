#include "game.h"
#include "isAttacked.h"
#include "isGameOver.h"
#include "isPathEmpty.h"
#include "move.h"
#include "piece.h"
#include "test.h"
#include "turnInfo.h"
#include "validMove.h"
#include <QLabel>
#include <QtTest/QTestEvent>
#include <QTestEventList>
#include <iostream>

int repition_test(Game &game)
{
    int total = 0;
    QTestEventList events;
    events.addMouseClick(Qt::LeftButton, Qt::NoModifier, QPoint(750,150), 10);
    events.addMouseClick(Qt::LeftButton, Qt::NoModifier, QPoint(550,250), 10);
    events.addMouseClick(Qt::LeftButton, Qt::NoModifier, QPoint(0,0), 10);
    events.simulate(&game);
    return total;
}

void test(Game &game)
{
    int total = 0;
    total += repition_test(game);
    std::cout << total << " tests passed." << std::endl;
}
