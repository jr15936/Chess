#ifndef TEST_MOVE_H
#define TEST_MOVE_H

#include "piece.h"

//basically does the same thing as move.h but without the visual representation
void testMove(std::vector<std::vector<TestPiece>> &board, size_t cur_pos_i, size_t cur_pos_j, size_t new_pos_i, size_t new_pos_j);

#endif // TEST_MOVE_H
