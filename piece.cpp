#include "piece.h"

Piece::Piece(bool e, char c, char t, QWidget *parent) : QLabel (parent)
{
    empty = e;
    colour = c;
    type = t;

    if (empty) {
        setPixmap(QPixmap("://resources/empty.png"));
    }
    else if (c == 'w') {
        switch (t) {
        case 'k':
            setPixmap(QPixmap("://resources/king_white.png"));
            break;
        case 'q':
            setPixmap(QPixmap("://resources/queen_white.png"));
            break;
        case 'r':
            setPixmap(QPixmap("://resources/rook_white.png"));
            break;
        case 'b':
            setPixmap(QPixmap("://resources/bishop_white.png"));
            break;
        case 'n':
            setPixmap(QPixmap("://resources/knight_white.png"));
            break;
        case 'p':
            setPixmap(QPixmap("://resources/pawn_white.png"));
            break;
        }
    }
    else if (c == 'b') {
        switch (t) {
        case 'k':
            setPixmap(QPixmap("://resources/king_black.png"));
            break;
        case 'q':
            setPixmap(QPixmap("://resources/queen_black.png"));
            break;
        case 'r':
            setPixmap(QPixmap("://resources/rook_black.png"));
            break;
        case 'b':
            setPixmap(QPixmap("://resources/bishop_black.png"));
            break;
        case 'n':
            setPixmap(QPixmap("://resources/knight_black.png"));
            break;
        case 'p':
            setPixmap(QPixmap("://resources/pawn_black.png"));
            break;
        }
    }
}

void Piece::setEmpty()
{
    empty = true;
    colour = '-';
    type = '-';
    setPixmap(QPixmap("://resources/emtpy.png"));
}

void TestPiece::setEmpty()
{
    empty = true;
    colour = '-';
    type = '-';
}

void Piece::setAs(Piece *piece)
{
    this->empty = piece->empty;
    this->colour = piece->colour;
    this->type = piece->type;
    this->setPixmap(*piece->pixmap());
}

bool operator==(const TestPiece &p1, const TestPiece &p2)
{
    return (p1.colour == p2.colour && p1.type == p2.type);
}
