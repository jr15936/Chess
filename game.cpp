#include <QtWidgets>
#include "piece.h"
#include "game.h"
#include "move.h"
#include "validMove.h"
#include "isGameOver.h"
#include "sidebar.h"

#include <iostream>

Game::Game(QWidget *parent) : QFrame(parent)
{
    normal = true;
    cur_pos = new QPoint(-1000,-1000);
    new_pos = new QPoint(-1000,-1000);
    cur_h = new QLabel(this);
    new_h = new QLabel(this);
    last_h = new QLabel(this);
    cur_h->setStyleSheet(".QLabel  { background-color : rgba(255,255,0,30%);}");
    new_h->setStyleSheet(".QLabel  { background-color : rgba(255,255,0,30%);}");
    last_h->setStyleSheet(".QLabel { background-color : rgba(255,255,0,30%);}");
    cur_h->setGeometry(-1000,-1000,100,100);
    new_h->setGeometry(-1000,-1000,100,100);
    last_h->setGeometry(-1000,-1000,100,100);
    turn_no = 0;
    fifty_move = 0;
    current_player = 'w';
    black_kingside = true;
    black_queenside = true;
    white_kingside = true;
    white_queenside = true;
    white_king_pos = new QPoint(400,700);
    black_king_pos = new QPoint(400,0);
    end_msg = nullptr;
    hint_cur = new QLabel(this);
    hint_new = new QLabel(this);
    hint_cur->setGeometry(-1000,-1000,100,100);
    hint_new->setGeometry(-1000,-1000,100,100);
    hint_cur->setStyleSheet(".QLabel  { background-color : rgba(255,0,0,30%);}");
    hint_new->setStyleSheet(".QLabel  { background-color : rgba(255,0,0,30%);}");

    Piece *king_white = new Piece(false,'w', 'k', this);
    Piece *queen_white = new Piece(false,'w', 'q', this);
    Piece *rook_white1 = new Piece(false,'w', 'r', this);
    Piece *rook_white2 = new Piece(false,'w', 'r', this);
    Piece *bishop_white1 = new Piece(false,'w', 'b', this);
    Piece *bishop_white2 = new Piece(false,'w', 'b', this);
    Piece *Knight_white1 = new Piece(false,'w', 'n', this);
    Piece *Knight_white2 = new Piece(false,'w', 'n', this);

    std::vector<Piece *> white_row {rook_white1, Knight_white1, bishop_white1, queen_white,
                                    king_white, bishop_white2, Knight_white2, rook_white2};

    Piece *king_black = new Piece(false,'b', 'k', this);
    Piece *queen_black = new Piece(false,'b', 'q', this);
    Piece *rook_black1 = new Piece(false,'b', 'r', this);
    Piece *rook_black2 = new Piece(false,'b', 'r', this);
    Piece *bishop_black1 = new Piece(false,'b', 'b', this);
    Piece *bishop_black2 = new Piece(false,'b', 'b', this);
    Piece *Knight_black1 = new Piece(false,'b', 'n', this);
    Piece *Knight_black2 = new Piece(false,'b', 'n', this);

    std::vector<Piece *> black_row {rook_black1, Knight_black1, bishop_black1, queen_black,
                                    king_black, bishop_black2, Knight_black2, rook_black2};

    std::vector<Piece *> white_pawns;
    std::vector<Piece *> black_pawns;
    for (int i = 0; i < 8; ++i) {
        Piece *white_pawn = new Piece(false,'w','p',this);
        white_pawns.push_back(white_pawn);
        Piece *black_pawn = new Piece(false,'b','p',this);
        black_pawns.push_back(black_pawn);
    }

    std::vector<Piece *> empty_row1, empty_row2, empty_row3, empty_row4;
    for (int i = 0; i < 8; ++i) {
        Piece *empty1 = new Piece(true,'-','-',this);
        empty_row1.push_back(empty1);
        Piece *empty2 = new Piece(true,'-','-',this);
        empty_row2.push_back(empty2);
        Piece *empty3 = new Piece(true,'-','-',this);
        empty_row3.push_back(empty3);
        Piece *empty4 = new Piece(true,'-','-',this);
        empty_row4.push_back(empty4);
    }

    board_pieces = std::vector<std::vector<Piece *>> {black_row, black_pawns, empty_row1, empty_row2,
                                                      empty_row3, empty_row4, white_pawns, white_row};
    for (auto r : board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        test_board.push_back(temp);
    }

    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            board_pieces[j][i]->setGeometry(int(i*100), int(j*100), 100, 100);
        }
    }
    turn_list.push_back(TurnInfo(TestPiece(), cur_pos, new_pos, test_board, QString(""), black_kingside, black_queenside, white_kingside, white_queenside));

}

void Game::resetGame()
{
    if (end_msg != nullptr) {
        end_msg->close();
    }
    *cur_pos = QPoint(-1000,-1000);
    *new_pos = QPoint(-1000,-1000);
    cur_h->setGeometry(-1000,-1000,100,100);
    new_h->setGeometry(-1000,-1000,100,100);
    last_h->setGeometry(-1000,-1000,100,100);
    turn_no = 0;
    fifty_move = 0;
    current_player = 'w';
    black_kingside = true;
    black_queenside = true;
    white_kingside = true;
    white_queenside = true;
    *white_king_pos = QPoint(400,700);
    *black_king_pos = QPoint(400,0);
    hint_cur->setGeometry(-1000,-1000,100,100);
    hint_new->setGeometry(-1000,-1000,100,100);

    for (size_t i = 0; i < 8; ++i) {
        board_pieces[1][i]->setAs(new Piece(false, 'b', 'p', this));
        board_pieces[2][i]->setEmpty();
        board_pieces[3][i]->setEmpty();
        board_pieces[4][i]->setEmpty();
        board_pieces[5][i]->setEmpty();
        board_pieces[6][i]->setAs(new Piece(false, 'w', 'p', this));
    }

    board_pieces[0][0]->setAs(new Piece(false, 'b', 'r', this));
    board_pieces[0][1]->setAs(new Piece(false, 'b', 'n', this));
    board_pieces[0][2]->setAs(new Piece(false, 'b', 'b', this));
    board_pieces[0][3]->setAs(new Piece(false, 'b', 'q', this));
    board_pieces[0][4]->setAs(new Piece(false, 'b', 'k', this));
    board_pieces[0][5]->setAs(new Piece(false, 'b', 'b', this));
    board_pieces[0][6]->setAs(new Piece(false, 'b', 'n', this));
    board_pieces[0][7]->setAs(new Piece(false, 'b', 'r', this));

    board_pieces[7][0]->setAs(new Piece(false, 'w', 'r', this));
    board_pieces[7][1]->setAs(new Piece(false, 'w', 'n', this));
    board_pieces[7][2]->setAs(new Piece(false, 'w', 'b', this));
    board_pieces[7][3]->setAs(new Piece(false, 'w', 'q', this));
    board_pieces[7][4]->setAs(new Piece(false, 'w', 'k', this));
    board_pieces[7][5]->setAs(new Piece(false, 'w', 'b', this));
    board_pieces[7][6]->setAs(new Piece(false, 'w', 'n', this));
    board_pieces[7][7]->setAs(new Piece(false, 'w', 'r', this));

    resetSidebarSignal();

    test_board = std::vector<std::vector<TestPiece>>();
    for (auto r : board_pieces) {
        std::vector<TestPiece> temp;
        for (auto p : r) {
            temp.push_back(TestPiece(p->empty, p->colour, p->type));
        }
        test_board.push_back(temp);
    }
    turn_list = std::vector<TurnInfo>();
    turn_list.push_back(TurnInfo(TestPiece(), cur_pos, new_pos, test_board, QString(""), black_kingside, black_queenside, white_kingside, white_queenside));
}

void Game::showHint()
{
    std::vector<QPoint *> points = findValidMove(*this);
    if (points.empty())
        return;

    hint_cur->setGeometry(points[0]->rx(),points[0]->ry(),100,100);
    hint_new->setGeometry(points[1]->rx(),points[1]->ry(),100,100);
}

void Game::prevTurn()
{
    if (turn_no < 1)
        return;
    if (end_msg != nullptr) {
        end_msg->close();
    }
    --turn_no;
    auto it = turn_list.begin() + turn_no;
    test_board = (*it).board_pieces;

    int no_of_pieces_before = 0;
    int no_of_pieces_after = 0;
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (!board_pieces[i][j]->empty) {
                ++no_of_pieces_before;
            }
            if (!test_board[i][j].empty) {
                ++no_of_pieces_after;
            }

        }
    }
    if (no_of_pieces_after != no_of_pieces_before) {
        prevSidebarSignal(current_player);
    }

    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            board_pieces[i][j]->setAs(new Piece(test_board[i][j].empty, test_board[i][j].colour, test_board[i][j].type));
        }
    }

    if (current_player == 'w') {
        moveListSignal(QString(""),-2);
    }
    else {
        moveListSignal(QString(""),-1);
    }

    black_kingside = it->castle_bools[0];
    black_queenside = it->castle_bools[1];
    white_kingside = it->castle_bools[2];
    white_queenside = it->castle_bools[3];
    nextPlayer();
}

void Game::nextTurn()
{
    if (size_t(turn_no) >= turn_list.size() - 1)
        return;
    ++turn_no;
    auto it = turn_list.begin() + turn_no;
    test_board = (*it).board_pieces;

    int no_of_pieces_before = 0;
    int no_of_pieces_after = 0;
    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            if (!board_pieces[i][j]->empty) {
                ++no_of_pieces_before;
            }
            if (!test_board[i][j].empty) {
                ++no_of_pieces_after;
            }

        }
    }
    if (no_of_pieces_after != no_of_pieces_before) {
        nextSidebarSignal(current_player);
    }

    for (size_t i = 0; i < 8; ++i) {
        for (size_t j = 0; j < 8; ++j) {
            board_pieces[i][j]->setAs(new Piece(test_board[i][j].empty, test_board[i][j].colour, test_board[i][j].type));
        }
    }

    moveListSignal(it->moveText, turn_no-1);

    black_kingside = it->castle_bools[0];
    black_queenside = it->castle_bools[1];
    white_kingside = it->castle_bools[2];
    white_queenside = it->castle_bools[3];
    nextPlayer();
    isGameOver(*this);
}

void Game::nextPlayer()
{
    if (current_player == 'w') {
        current_player = 'b';
    }
    else {
        current_player = 'w';
    }
}

void Game::printBoard()
{
    int k = 8;
    std::cout << "    a b c d e f g h\n   _________________\n";
    for (auto i : this->test_board) {
        std::cout << k << " |";
        for (auto j : i) {
            std::cout << ' ' << j.type;
        }
        std::cout << " | ";
        std::cout << k << "\n";
        --k;
    }
    std::cout << "  |_________________|\n    a b c d e f g h\n";
    std::cout << std::endl;
}

void Game::turn()
{
    performMove(*this, cur_pos, new_pos);
    ++turn_no;
    ++fifty_move;
    nextPlayer();
}


void Game::mousePressEvent(QMouseEvent *event)
{
    if (!normal)
        return;
    if (event->pos().rx() > 800) {
        return;
    }

    if (*cur_pos == QPoint(-1000,-1000)) {
        QPoint temp = QPoint(event->pos());

        if (!(this->board_pieces[size_t(temp.ry()/100)][size_t(temp.rx()/100)]->empty)) {
            *cur_pos = temp;
            cur_h->setGeometry(int(temp.rx()/100)*100, int(temp.ry()/100)*100, 100, 100);
        }

        return;
    }

    *new_pos = QPoint(event->pos());

    if (validMove(*this, cur_pos, new_pos)) {
        last_h->setGeometry(cur_h->geometry());
        new_h->setGeometry(int(new_pos->rx()/100)*100, int(new_pos->ry()/100)*100, 100, 100);
        turn();
        isGameOver(*this);
    }

    *new_pos = QPoint(-1000,-1000);
    *cur_pos = QPoint(-1000,-1000);
    cur_h->setGeometry(-1000,-1000,100,100);
    hint_cur->setGeometry(-1000,-1000,100,100);
    hint_new->setGeometry(-1000,-1000,100,100);
    return;
}
